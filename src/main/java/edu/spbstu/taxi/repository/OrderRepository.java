package edu.spbstu.taxi.repository;

import edu.spbstu.taxi.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
Optional<Order> findById(Integer id);
}
