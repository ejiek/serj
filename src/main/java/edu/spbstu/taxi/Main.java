package edu.spbstu.taxi;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@SpringBootApplication
@EnableJpaRepositories
@PropertySource(value = { "classpath:application.properties" })
@EntityScan(basePackages = "edu.spbstu.taxi")

public class Main {

        @Autowired
        private Environment env;

       @Bean
       public DataSource dataSource() {
           DriverManagerDataSource dataSource = new DriverManagerDataSource();
           dataSource.setDriverClassName(env.getRequiredProperty("spring.datasource.driver-class-name"));
           dataSource.setUrl(env.getRequiredProperty("spring.datasource.url"));
           dataSource.setUsername(env.getRequiredProperty("spring.datasource.username"));
           dataSource.setPassword(env.getRequiredProperty("spring.datasource.password"));
           return dataSource;
       }

       @Bean
       public JdbcTemplate jdbcTemplate(DataSource dataSource) {
           JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
           jdbcTemplate.setResultsMapCaseInsensitive(true);
           return jdbcTemplate;
       }

       public Properties additionalProreties() {
           Properties properties = new Properties();
   //        properties.setProperty("hibernate.show_sql", "true");
           return properties;
       }

       @Bean
       public EntityManagerFactory entityManagerFactory() {

           HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
           vendorAdapter.setGenerateDdl(true);

           LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
           factory.setJpaVendorAdapter(vendorAdapter);
           factory.setPackagesToScan("edu.spbstu.taxi");
           factory.setDataSource(dataSource());
           factory.setPersistenceUnitName("taxi");
           factory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
           factory.setJpaProperties(additionalProreties());
           factory.afterPropertiesSet();
           return factory.getObject();
       }

    public static void main(String[] args) {
        //Login login = new Login();
        //login.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        SpringApplication.run(Main.class, args);
    }
}
